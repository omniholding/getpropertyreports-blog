<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Shapely
 */

?>

            </div><!-- row -->
		</div><!-- #main -->
	</section><!-- section -->

    <?php shapely_footer_callout(); ?>

    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <div class="footer-all-wrap">
                    <div class="clearfix">
                        <div class="footer-left-wrap">
                            <img class="footer-logo" src="http://getpropertyreports.com/blog/img/logo.png">
                        </div>
                        <div class="footer-right-wrap">
                            <p class="footer-header">Providing you with the most reliable real estate records on the market.</p>
                            <p class="footer-text">Get Property Reports is the number one resource for accurate and insightful property reports.</p>
                            <ul class="footer-list">
                                <li>
                                    <a href="index.html">Home</a>
                                </li>
                                <li>
                                    <a href="about.html">About</a>
                                </li>
                                <li>
                                    <a href="pricing.html">Pricing</a>
                                </li>
                                <li>
                                    <a href="contact.html">Contact</a>
                                </li>
                                <li>
                                    <a href="login.html">Sign In</a>
                                </li>
                                <li>
                                    <a href="/blog">Blog</a>
                                </li>
                                <li>
                                    <a>Terms of Use</a>
                                </li>
                                <li>
                                    <a>Privacy Policy</a>
                                </li>
                            </ul>
                            <p class="copyright">Copyright © 2016 GetPropertyReports.com All rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

	<!-- <footer id="colophon" class="site-footer footer bg-dark" role="contentinfo">
      <div class="container footer-inner">
        <div class="row">
          <?php get_sidebar( 'footer' ); ?>
        </div>

        <div class="row">
          <div class="site-info col-sm-6">
            <div class="copyright-text"><?php _e( get_theme_mod( 'shapely_footer_copyright'), 'shapely' ); ?></div>
            <div class="footer-credits"><?php shapely_footer_info(); ?></div>
          </div>
          <div class="col-sm-6 text-right">
            <?php if( !get_theme_mod('footer_social') ) shapely_social_icons(); ?>
          </div>
        </div>
      </div>

      <a class="btn btn-sm fade-half back-to-top inner-link" href="#top"><i class="fa fa-angle-up"></i></a>
    </footer> -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
